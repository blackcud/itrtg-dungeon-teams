from implementations.v1.DungeonScorerForestD2 import DungeonScorerForestD2
from implementations.v1.DungeonScorerScrapyardD1 import DungeonScorerScrapyardD1
from implementations.v1.DungeonScorerVolcanoD2 import DungeonScorerVolcanoD2
from implementations.v1.DungeonScorerWaterTempleD2 import DungeonScorerWaterTempleD2

from basemodel.tools import pet_export_csv_to_pet_set, print_pet_set_stats, remove_pets_by_name, remove_pets_by_job, remove_pets_by_owned_status, calculate_number_of_team_combinations, perform_sequential_run

# Common Options:
FILE_PATH_PET_EXPORT = 'petexport.csv'
TEAM_SIZE = 6
PET_IGNORE_LIST = {'Yggdrasil', 'Gray', 'Bag', 'Pandora', 'Chocobear', 'FSM', 'Nightmare', 'Pumpkin', 'AfkyClone', 'Stone', 'Crab'}
EXCLUDE_ALCHEMISTS = True
EXCLUDE_ADVENTURERS = True
ONLY_USE_OWNED_PETS = True

# Generate Various Sub-Pet-Sets to reduce execution times:
pet_set = pet_export_csv_to_pet_set(FILE_PATH_PET_EXPORT)
pet_set_reduced = pet_set.copy()
if len(PET_IGNORE_LIST) >= 0:
    pet_set_reduced = remove_pets_by_name(pet_set_reduced, PET_IGNORE_LIST)
if EXCLUDE_ALCHEMISTS:
    pet_set_reduced = remove_pets_by_job(pet_set_reduced, {'Alchemist'})
if EXCLUDE_ADVENTURERS:
    pet_set_reduced = remove_pets_by_job(pet_set_reduced, {'Adventurer'})
if ONLY_USE_OWNED_PETS:
    pet_set_reduced = remove_pets_by_owned_status(pet_set_reduced, desired_owned_status=True)
print_pet_set_stats(pet_set, description="All Pets")
print_pet_set_stats(pet_set_reduced, description="All Pets (reduced)")
optimisation_percent = 100 - (round(calculate_number_of_team_combinations(pet_set_reduced) / calculate_number_of_team_combinations(pet_set) * 100))
print("Info: the reduced pet_set is " + str(optimisation_percent) + "% smaller!")

perform_sequential_run(pet_set=pet_set_reduced, dungeons=[DungeonScorerVolcanoD2, DungeonScorerWaterTempleD2, DungeonScorerForestD2, DungeonScorerScrapyardD1])
