import math
import time
from collections import Counter

import pandas as pd
from tqdm import tqdm

from basemodel import dungeonScorer
from basemodel.pet import Pet


def perform_run(dungeon_class: dungeonScorer, dungeon_name, pet_set: set, team_size: int = 6, report_intermediate_winners: bool = True):
    print("*" * 20)
    print("START: " + dungeon_name)
    print("*" * 20)
    temp_dungeon = dungeon_class(pet_set=pet_set, team_size=team_size, dungeon_name=dungeon_name)
    print("Number of pets in pool:\t" + str(len(temp_dungeon.pet_set)))
    print("Number of combinations:\t" + str(temp_dungeon.combinations()))

    best_team = {
        'index': -1,
        'pets': None,
        'score': -9999999999999999
    }
    time_start = time.time()
    for scores in tqdm(temp_dungeon.generateScores(), total=temp_dungeon.combinations()):
        if scores[2] > best_team['score']:
            if report_intermediate_winners:
                print("\n" + str(scores[0]) + str(".: ") + str([str(p) for p in scores[1]]) + str(" SCORE: ") + str(scores[2]))
            best_team['index'] = scores[0]
            best_team['pets'] = scores[1]
            best_team['score'] = scores[2]
    time_end = time.time()
    time_total_seconds = math.ceil(time_end - time_start)
    print("\nRun complete!")
    print("Combos:\t" + str(temp_dungeon.combinations()))
    print("Time:\t" + str(time_total_seconds))
    print("Speed:\t" + str(round(temp_dungeon.combinations() / time_total_seconds)))
    print("Best Team:")
    print(str([str(p) for p in best_team['pets']]))
    print("*" * 20)
    print("END: " + dungeon_name)
    print("*" * 20)

    return best_team['pets'], best_team['score']


def perform_sequential_run(pet_set, dungeons: [dungeonScorer]):
    print("Performing Sequential Run:")
    winning_teams = list()
    for i, dungeon in enumerate(dungeons):
        print(str(i) + ":" + dungeon.__name__)

    remaining_pets = pet_set.copy()
    for i, dungeon in enumerate(dungeons):
        print("Starting Sequential Run #" + str(i))
        best_team, score = perform_run(dungeon_class=dungeon, pet_set=remaining_pets, dungeon_name=str(dungeon.__name__) + "#" + str(i), report_intermediate_winners=False)
        winning_teams.append((best_team, score))
        remaining_pets = remove_pets_by_team(remaining_pets, best_team)

    for i, result in enumerate(zip(winning_teams, dungeons)):
        print("run #", str(i), ":", result[1].__name__, [str(p) for p in result[0][0]])


def pet_export_csv_to_pet_set(filename='petexport.csv', sep='\t'):
    pet_df = pd.read_csv(filepath_or_buffer=filename, sep=sep)
    pet_set = set()

    for index, row in pet_df.iterrows():
        pet_set.add(Pet(name=row['Name'], element=row['Element'], job=row['Class'], dungeon_level=row['Dungeon Level'], job_level=row['Class Level'], hp=row['HP']))

    return pet_set


def print_pet_set_stats(pet_set: set, description: str):
    job_stats = Counter()
    element_stats = Counter()
    for p in pet_set:
        job_stats[p.job] += 1
        element_stats[p.element] += 1
    total_entries = len(pet_set)
    print(description + "(" + str(total_entries) + "):")
    print("\t" + ", ".join([str(x[0]) + " x" + str(x[1]) for x in sorted(job_stats.items())]))
    print("\t" + ", ".join([str(x[0]) + " x" + str(x[1]) for x in sorted(element_stats.items())]))


def remove_pets_by_name(pet_set: set, names_to_remove: set):
    filtered_set = set()
    for p in pet_set:
        if p.name not in names_to_remove:
            filtered_set.add(p)
    return filtered_set


def remove_pets_by_job(pet_set: set, jobs_to_remove: set):
    filtered_set = set()
    for p in pet_set:
        if p.job not in jobs_to_remove:
            filtered_set.add(p)
    return filtered_set


def remove_pets_by_element(pet_set: set, elements_to_remove: set):
    filtered_set = set()
    for p in pet_set:
        if p.element not in elements_to_remove:
            filtered_set.add(p)
    return filtered_set


def remove_pets_by_owned_status(pet_set: set, desired_owned_status: bool):
    filtered_set = set()
    for p in pet_set:
        if p.isOwned() is desired_owned_status:
            filtered_set.add(p)
    return filtered_set


def remove_pets_by_team(pet_set: set, team_to_remove: set):
    filtered_set = pet_set.copy()
    for ptr in team_to_remove:
        filtered_set = remove_pets_by_name(filtered_set, ptr.name)
    return filtered_set


def calculate_number_of_team_combinations(pet_set: set, TEAM_SIZE: int = 6):
    return math.comb(len(pet_set), TEAM_SIZE)
