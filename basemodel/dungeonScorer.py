import itertools
from collections import Counter

from basemodel.tools import calculate_number_of_team_combinations


class DungeonScorer(object):
    SCORES_EVENT_BASE = 3024  # Events that we have to cover. This value also serves as a baseline to bring things into relation

    SCORES_ELEMENT_MATCHING = SCORES_EVENT_BASE / 6 / 7  # The primary element of the dungeon
    SCORES_ELEMENT_EFFECTIVE = SCORES_EVENT_BASE / 6 / 8  # The element that is effective against the primary element
    SCORES_ELEMENT_BAD = SCORES_EVENT_BASE / 6 / 2 * -1  # The element that takes extra damage from the primary element

    SCORES_DPS_EFFECTIVE_ELEMENT = SCORES_EVENT_BASE  # A mage or assassin in the group is of the effective element

    SCORES_JOB_DEFENDER_PRESENT = SCORES_EVENT_BASE  # Having at least one defender is considered mandatory
    SCORES_JOB_SUPPORTER_PRESENT = SCORES_EVENT_BASE  # Having at least one supporter is considered mandatory
    SCORES_JOB_MAGE_PRESENT = SCORES_EVENT_BASE  # Mages are good to have because area damage
    SCORES_JOB_ROGUE_PRESENT = SCORES_EVENT_BASE * 2  # Rogues are good to have because loot
    SCORES_JOB_ASSASSIN_PRESENT = SCORES_EVENT_BASE  # Assassins provide strong single target damage which is only relevant for strong bosses
    SCORES_JOB_BLACKSMITH_PRESENT = SCORES_EVENT_BASE / 2  # Blacksmiths provide a damage boost to the whole party
    SCORES_JOB_UNIQUENESS_BONUS = SCORES_EVENT_BASE / 30  # Quadratic bonus which can be used to reward uniqueness among classes/jobs
    SCORES_JOB_NONE_PENALTY = SCORES_EVENT_BASE * 2 * -1  # Using pets without a class/job is not good

    SCORES_FAIL_T1 = SCORES_EVENT_BASE * 10 * -1  # Massive penalty Tier 1
    SCORES_FAIL_T2 = SCORES_EVENT_BASE * 100 * -1  # Massive penalty Tier 2
    SCORES_FAIL_T3 = SCORES_EVENT_BASE * 1000 * -1  # Massive penalty Tier 3
    SCORES_FAIL_T4 = SCORES_EVENT_BASE * 10000 * -1  # Massive penalty Tier 4
    SCORES_FAIL_T5 = SCORES_EVENT_BASE * 100000 * -1  # Massive penalty Tier 5
    SCORES_FAIL_T6 = SCORES_EVENT_BASE * 1000000 * -1  # Massive penalty Tier 6

    SCORES_FAIL_EVENT_D1 = SCORES_FAIL_T3
    SCORES_FAIL_EVENT_D2 = SCORES_FAIL_T4
    SCORES_FAIL_EVENT_D3 = SCORES_FAIL_T5

    job_present = {
        'Defender': SCORES_JOB_DEFENDER_PRESENT,
        'Supporter': SCORES_JOB_SUPPORTER_PRESENT,
        'Mage': SCORES_JOB_MAGE_PRESENT,
        'Rogue': SCORES_JOB_ROGUE_PRESENT,
        'Assassin': SCORES_JOB_ASSASSIN_PRESENT,
        'Blacksmith': SCORES_JOB_BLACKSMITH_PRESENT
    }

    def __init__(self, pet_set: set, dungeon_name: str, team_size: int = 6):
        self.pet_set = pet_set
        self.dungeon_name = dungeon_name
        self.team_size = team_size

    def generateScores(self):
        for i, team in enumerate(itertools.combinations(self.pet_set, self.team_size)):
            yield i, team, self.calculate_score(team)

    def combinations(self):
        return calculate_number_of_team_combinations(self.pet_set, self.team_size)

    @staticmethod
    def calculate_score(team):
        return 0

    @staticmethod
    def countElementsAndJobs(team, opposite_element):
        job_stats = Counter()
        element_stats = Counter()
        strong_mage = 0
        for p in team:
            job_stats[p.job] += 1
            element_stats[p.element] += 1
            if p.job == 'Mage' and p.element == opposite_element:
                strong_mage += 1

        return element_stats, job_stats, strong_mage

    @staticmethod
    def calculateGenericElementAndJobBonuses(element_stats, job_stats, matching_element, opposite_element, bad_element):
        modifier = 0
        modifier += (element_stats[matching_element] + element_stats['Neutral']) * DungeonScorer.SCORES_ELEMENT_MATCHING
        modifier += (element_stats[opposite_element] + element_stats['Neutral']) * DungeonScorer.SCORES_ELEMENT_EFFECTIVE
        modifier += element_stats[bad_element] * DungeonScorer.SCORES_ELEMENT_BAD
        modifier += job_stats['None'] * DungeonScorer.SCORES_JOB_NONE_PENALTY
        for job, bonus in DungeonScorer.job_present.items():
            if job_stats[job] >= 1:
                modifier += bonus
        return modifier

    @staticmethod
    def calculateJobUniquenessBonus(job_stats):
        number_of_different_jobs = len(job_stats)
        if 'None' in job_stats.keys():  # Not having a job is not considered a unique job here
            number_of_different_jobs -= 1
        return number_of_different_jobs * number_of_different_jobs * DungeonScorer.SCORES_JOB_UNIQUENESS_BONUS
