from basemodel.enums import PET_ELEMENT, PET_JOB


class Pet(object):
    def __init__(self, name: str, element: PET_ELEMENT, dungeon_level: int, job_level: int, hp: int, job: PET_JOB = None, intended_job: PET_JOB = None, weight: float = 1.0):
        self.name = name
        self.job = job
        self.element = element
        self.intended_job = intended_job
        self.weight = weight
        self.dungeon_level = dungeon_level
        self.job_level = job_level
        self.hp = int(hp.replace(",", ""))

    def isOwned(self):
        return self.dungeon_level != 1 or self.job_level != 0

    def isEvoed(self):
        return self.job is not None

    def __str__(self):
        proper_name = str(self.name)
        proper_element = str(self.element).replace("PetElement.", "")
        proper_job = str(self.job).replace("PetJob.", "")
        return proper_name + " (" + proper_element + " " + proper_job + ")"

    def __lt__(self, other):
        return self.name < other.name
