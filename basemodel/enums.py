from enum import Enum

PET_ELEMENT = Enum('PetElement', 'Neutral Water Fire Wind Earth Dark Light Flex')
PET_JOB = Enum('PetJob', 'Adventurer Alchemist Assassin Blacksmith Defender Mage Rogue Supporter Flex')
