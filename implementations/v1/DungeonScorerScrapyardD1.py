from basemodel.dungeonScorer import DungeonScorer


class DungeonScorerScrapyardD1(DungeonScorer):
    @staticmethod
    def calculate_score(team):
        score = 0
        element_stats, job_stats, strong_mage = DungeonScorer.countElementsAndJobs(team, opposite_element='Neutral')

        # Strict Requirements for this dungeon:
        if job_stats['Rogue'] < 1:  # cover event cursed chest
            score += DungeonScorer.SCORES_FAIL_EVENT_D1

        # Strict Group Comp Requirements:
        if job_stats['Defender'] != 1:  # exactly 1 defender
            score += DungeonScorer.SCORES_FAIL_T2
        if job_stats['Supporter'] != 1:  # exactly 1 support
            score += DungeonScorer.SCORES_FAIL_T3
        if strong_mage == 0:  # not having a matching mage is very bad
            score += DungeonScorer.SCORES_FAIL_T2
        if job_stats['Mage'] > 1:  # max 1 Mage
            score += DungeonScorer.SCORES_FAIL_T2

        # Grant small bonuses for matching elements and having various classes
        score += DungeonScorer.calculateGenericElementAndJobBonuses(element_stats=element_stats, job_stats=job_stats, matching_element='Fire', opposite_element='Water', bad_element='Wind')
        score += DungeonScorer.SCORES_DPS_EFFECTIVE_ELEMENT * strong_mage

        # Prefer well rounded teams. This is probably a good idea, but we don't know for sure.
        score += DungeonScorer.calculateJobUniquenessBonus(job_stats=job_stats)

        return score
